
# not used: ASSUME_MOVIE_MAX_DURATION  = 60.0*90
MAX_NUM_EXTRACT_FRAMES      = 50      # number of frames to extract from each video
DEFAULT_NUM_EXTRACT_THREADS = 4       # max threads to be used extracting frames

# Values for movies
OFFSET_MOVIE_START_EXTRACT_FRAMES =   120.0   # seconds Offset after start of video
OFFSET_MOVIE_END_EXTRACT_FRAMES   =  -60.0+(15*60) # seconds offset before duration of video

# Values for series episodes (commonly 20-50mins)
ASSUME_SERIES_EPISODE_MAX_DURATION  = 60.0*50    # if duration lower (50mins), assume video is series episode
OFFSET_EPISODE_START_EXTRACT_FRAMES = 60.0*2    # shorter offset for episodes than movies
OFFSET_EPISODE_END_EXTRACT_FRAMES   = -60.0*2   # shorter offset for episodes than movies

# Values for short videos ( shorter than 20mins )
ASSUME_SHORTS_MAX_DURATION          = 60.0*20    # if duration lower (20mins), assume video is short clip
OFFSET_SHORTS_START_EXTRACT_FRAMES  = 2.0    # shorter offset for episodes than movies
OFFSET_SHORTS_END_EXTRACT_FRAMES    = -2.0   # shorter offset for episodes than movies


