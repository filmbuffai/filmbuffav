from . import *
import av

import datetime
#import ffmpeg
# import json
# from math import floor

# def getProbeJson(input_video):
#     """Return the video duration from ffmpeg probe json"""
#     return ffmpeg.probe(input_video)


# def getVideoDuration(input_video):
#     """Return the video duration from ffmpeg probe json"""
#     #video_info = ffmpeg.probe(input_video)['format']['duration']
#     input_video.duration
#     return float(video_info)



def getVideoDuration(video):
    """return video duration"""
    video = av.open(video,'r')
    fraction_secs = video.duration
    seconds = datetime.timedelta(seconds=fraction_secs)
    return  float(seconds.total_seconds() /1000/1000 )  # todo: is it smart to use float?


def getExtractTimes(input_movie, max_num_imgs: int = MAX_NUM_EXTRACT_FRAMES):
    """Calculate a list of timestamps of where to extract frames from"""
    video_duration = getVideoDuration(input_movie)
    if (video_duration / 60) < ASSUME_SHORTS_MAX_DURATION:
        offset_start = abs(OFFSET_SHORTS_START_EXTRACT_FRAMES)
        offset_end = -abs(OFFSET_SHORTS_END_EXTRACT_FRAMES)
    elif (video_duration / 60) < ASSUME_SERIES_EPISODE_MAX_DURATION:  # treat as series episode (~45mins) for shorter offsets
        offset_start = abs(OFFSET_EPISODE_START_EXTRACT_FRAMES)
        offset_end = -abs(OFFSET_EPISODE_END_EXTRACT_FRAMES)
    else:  # treat as feature film (~1.5hrs ++)
        offset_start = abs(OFFSET_MOVIE_START_EXTRACT_FRAMES)  # enforce positive
        offset_end = -abs(OFFSET_MOVIE_END_EXTRACT_FRAMES)  # enforce negative

    # seconds between extractions
    step_distance = (video_duration
                     - offset_start
                     + offset_end) / max_num_imgs

    timestamps = [round(offset_start, 2)]
    step = 0
    while step < (max_num_imgs - 2):
        # print(timestep)
        timestamp = round((timestamps[step] + step_distance), 2)  # todo:round to 2 decimals?
        step += 1
        timestamps.append(timestamp)
    timestamps.append( round(video_duration + offset_end, 2) )

    return timestamps

